# `KeyEvent.json`

JSON export of [`KeyEvent`](https://developer.android.com/reference/android/view/KeyEvent) key codes generated from [`android/platform/frameworks/base/core/java/android/view/KeyEvent.java`](https://android.googlesource.com/platform/frameworks/base/+/master/core/java/android/view/KeyEvent.java) by [`java-parser`](https://github.com/jhipster/prettier-java/tree/main/packages/java-parser).

## Usage

```js
import KeyEvent from 'keyevent.json' assert { type: 'json' };
console.log(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE); // -> 85
```