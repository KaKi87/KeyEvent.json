import { writeFile } from 'node:fs/promises';

import { parse } from 'java-parser';

await writeFile(
    './mod.json',
    JSON.stringify(
        parse(
            Buffer.from(
                await (await fetch('https://android.googlesource.com/platform/frameworks/base/+/master/core/java/android/view/KeyEvent.java?format=text')).text(),
                'base64'
            ).toString()
        )['children']['ordinaryCompilationUnit'][0]['children']['typeDeclaration'][0]['children']['classDeclaration'][0]['children']['normalClassDeclaration'][0]['children']['classBody'][0]['children']['classBodyDeclaration']
            .reduce((KeyEvent, _) => {
                _ = _['children']['classMemberDeclaration']?.[0]['children']['fieldDeclaration']?.[0]['children']['variableDeclaratorList'][0]['children']['variableDeclarator'][0]['children'];
                const
                    key = _?.['variableDeclaratorId'][0]['children']['Identifier'][0]['image'],
                    value = _?.['variableInitializer']?.[0]['children']['expression'][0]['children']['ternaryExpression'][0]['children']['binaryExpression'][0]['children']['unaryExpression'][0]['children']['primary'][0]['children']['primaryPrefix'][0]['children']['literal']?.[0]['children']['integerLiteral']?.[0]['children']['DecimalLiteral']?.[0]['image'];
                return {
                    ...KeyEvent,
                    ...key?.startsWith('KEYCODE_') ? {
                        [key]: value
                    } : {}
                };
            }, {})
    )
);